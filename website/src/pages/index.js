import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
    {
        title: 'Established',
        imageUrl: 'img/tabard.png',
        description: (
            <>
                Achievement was founded on June 9th, 2019. Originally we started
                on Alliance but eventually we moved to Horde on June 1st, 2020
                for a fresh beginning! Since then we have grown to over 100
                individual, active players!
            </>
        ),
    },
    {
        title: 'Engagement',
        imageUrl: 'img/tabard.png',
        description: (
            <>
                Achievement is a multi-faceted, social guild that strives to
                create a sense of community in all aspects of the game. We pride
                ourselves on providing a fun and engaging environment where
                players of all styles can find a home, make new friends, and
                always have something new to do.
            </>
        ),
    },
    {
        title: 'Objectives',
        imageUrl: 'img/tabard.png',
        description: (
            <>
                We have a heavy emphasis on achievement and mount hunting,
                farming reputations, collection and character completion.
                Additionally, we host monthly and weekly challenges and events
                to participate in!
            </>
        ),
    },
];

function Feature({ imageUrl, title, description }) {
    const imgUrl = useBaseUrl(imageUrl);
    return (
        <div className={clsx('col col--4', styles.feature)}>
            {imgUrl && (
                <div className="text--center">
                    <img
                        className={styles.featureImage}
                        src={imgUrl}
                        alt={title}
                    />
                </div>
            )}
            <h3>{title}</h3>
            <p>{description}</p>
        </div>
    );
}

export default function Home() {
    const context = useDocusaurusContext();
    const { siteConfig = {} } = context;
    return (
        <Layout
            title={`${siteConfig.title} WRA Guild`}
            description="World of Warcraft guild on Wyrmrest Accord server<head />"
        >
            <header className={clsx('hero hero--primary', styles.heroBanner)}>
                <div className="container">
                    <h1 className="hero__title">{siteConfig.title}</h1>
                    <p className="hero__subtitle">{siteConfig.tagline}</p>
                    <div className={styles.buttons}>
                        <Link
                            className={clsx(
                                'button button--secondary button--lg',
                                styles.getStarted,
                            )}
                            to={useBaseUrl('docs/')}
                        >
                            Learn More
                        </Link>
                    </div>
                </div>
            </header>
            <main>
                {features && features.length > 0 && (
                    <section className={styles.features}>
                        <div className="container">
                            <div className="row">
                                {features.map((props, idx) => (
                                    <Feature key={idx} {...props} />
                                ))}
                            </div>
                        </div>
                    </section>
                )}
            </main>
        </Layout>
    );
}
