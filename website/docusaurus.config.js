/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
    title: 'Achievement',
    tagline: 'Wyrmrest Accord',
    url: 'https://achievementwra.com/',
    baseUrl: '/',
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'warn',
    favicon: 'img/tabard_resize_Small.png',
    organizationName: 'achievement-guild', // Usually your GitHub org/user name.
    projectName: 'docusaurus', // Usually your repo name.
    themeConfig: {
        colorMode: {
            defaultMode: 'light',
            disableSwitch: false,
            respectPrefersColorScheme: true,
        },
        navbar: {
            title: 'Achievement',
            logo: {
                alt: 'Achievement Logo',
                src: 'img/tabard_resize_Medium.png',
            },
            items: [
                {
                    to: 'docs/',
                    activeBasePath: 'docs',
                    label: 'Information',
                    position: 'left',
                },
                // { to: 'blog', label: 'Blog', position: 'left' },
                // {
                //   href: 'https://gitlab.com/pages/docusaurus',
                //   label: 'GitLab',
                //   position: 'right',
                // },
            ],
        },
        footer: {
            style: 'dark',
            // links: [
            //   {
            //     title: 'Docs',
            //     items: [
            //       {
            //         label: 'Getting Started',
            //         to: 'docs/',
            //       },
            //     ],
            //   },
            //   {
            //     title: 'Community',
            //     items: [
            //       {
            //         label: 'Stack Overflow',
            //         href: 'https://stackoverflow.com/questions/tagged/docusaurus',
            //       },
            //       {
            //         label: 'Discord',
            //         href: 'https://discordapp.com/invite/docusaurus',
            //       },
            //       {
            //         label: 'Twitter',
            //         href: 'https://twitter.com/docusaurus',
            //       },
            //     ],
            //   },
            //   {
            //     title: 'More',
            //     items: [
            //       {
            //         label: 'Blog',
            //         to: 'blog',
            //       },
            //       {
            //         label: 'GitHub',
            //         href: 'https://github.com/facebook/docusaurus',
            //       },
            //     ],
            //   },
            // ],
            copyright: `Copyright © ${new Date().getFullYear()} Achievement. Built with Docusaurus.`,
        },
    },
    presets: [
        [
            '@docusaurus/preset-classic',
            {
                docs: {
                    sidebarPath: require.resolve('./sidebars.js'),
                    // Please change this to your repo.
                    editUrl:
                        'https://gitlab.com/achievement-guild/docusaurus/edit/master/website/',
                },
                blog: {
                    showReadingTime: true,
                    // Please change this to your repo.
                    editUrl:
                        'https://gitlab.com/achievement-guild/docusaurus/edit/master/website/',
                },
                theme: {
                    customCss: require.resolve('./src/css/custom.css'),
                },
            },
        ],
    ],
};
