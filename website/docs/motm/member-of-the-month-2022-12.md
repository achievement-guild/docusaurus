---
title: Member of the Month
---

## December

## Spnkmyr

#### Character Info

Tauren Beast Mastery Hunter

#### Favorite Mount

Scrapforged Mechaspider because it resembles an X-wing in flight. Or maybe the Magic Broom. I have a macro that says, "You're a wizard Harry" every time I get on.


#### Favorite Pet

I don't really collect pets, but probably the Sinister Squashling. Only because I bought 50 of them thinking I was going to make a killing on the AH, but it turned out to be a horrible investment and I somehow had to get rid of all of them.

#### Proudest Accomplishment

Probably completing the, "What a long, strange trip it's been" achievement. If you're on top of things, it only takes a year, but there was a time where I sporadically played wow, making this much longer than normal. I think I did this achievement off and on for like 10 years. No joke.

### GM/Officer Comment...

Spunkmeyer (yes, that’s his full name) always shows up ready for a good time – he’ll bring the jokes and witty the banter, even if that means he’s bantering with himself! He’s always positive, and he speaks really highly of all of his guild-mates. And as a hunter, he’s always got your back! (for us that meant taking a risk on a new guild we found on the internet - So without him we wouldn’t be here!)

_-Snibbletezz_


![Spnkmyr](https://cdn.discordapp.com/attachments/899907777311424542/1047978923415314514/image.png)