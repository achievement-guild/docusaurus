---
title: Member of the Month
---

## January

## Quipy

#### Character Info

Enhancement Shaman

#### Favorite Mount

Swift Zhevra Mount. Not sure if it is still available as it was an original “recruit a friend” reward mount. I do not care about its rarity, just that Soxhunt does not have it


#### Favorite Pet

I, unlike many WoW players, do not collect pets. I have pets, but have leveled less than a handful and avoid all pet battle World Quests.

#### Proudest Accomplishment

My Hand of A’dal title. Worked really hard back in Burning Crusade to get the Kael’thas Sunstrider kill in Tempest Keep before the title went away. Other than that are the Friends I made along the way.

### GM/Officer Comment...

Quipy has been both productive and beneficial member of the guild. He never shyes away from helping a member in need. He and Soxhunt joined together as a pair and have called Achievement home ever since. We are so glad to have them as part of our little corner of Azeroth.

_-Galafen_


![Spnkmyr](https://cdn.discordapp.com/attachments/748789679205122058/1063279348519546970/image.png)
