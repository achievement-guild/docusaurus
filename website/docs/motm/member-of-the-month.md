---
title: Member of the Month
---

## February

## Bajablastn (or whatever flavor of the week I'm on)
_Booj aka Bajablastn aka Bajasmashn aka Budder aka Chylev_

#### Character Info

Devastation Evoker

#### Favorite Mount

Shania (man she feels like a womammoth, SEE PIC)


#### Favorite Pet

Trollseeks the Cat

#### Proudest Accomplishment

Being a part of this guild, it has changed my mind set of different cultures, diversities and lifestyles. You have all made me a better player, and better human being. I am honored to be the February Flavor.

### GM/Officer Comment...

The thing about Booj, is that he’s not only the Achievement’s court jester, but he’s also a very skilled player. Not only is he a very skilled player, but he’s also consistently spending his time showing up for guildies. Booj bleeds Achievement. He is the first to pop into Discord to keep you company and he’s the first one to recognize when someone thing is off. If he knows you’re having a bad day, he will jump into your messages faster than it takes him to post a video of him dancing on Youtube. Yeah, he’s a lot, but he’s also ours, and he wouldn’t have it any other way. We love you Booj.

_-Braedy_


![Bajablastn](https://cdn.discordapp.com/attachments/872670822391763044/1070919723476328549/image.png)
