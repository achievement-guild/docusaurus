---
title: Community Values
---

![wow-scroll](/img/wowscroll.png)

### Respect and Inclusivity

We value our fellow community members and treat them as we would want to be
treated.

We appreciate both new and seasoned players and embrace their different levels
of experience and the content they create and share.

### What this Means

Spreading harmful hate speech will not be tolerated.

Trolling/Spamming chat, images or DMs is unnecessary and to be avoided. Nudity
is not allowed in the guild or in the discord server. Inappropriate pictures,
files, songs or posts will be removed and inappropriate profile pictures and
nicknames will be changed.

Members of all backgrounds are a part of our community. As such, racial jokes or
jokes about sexuality, political views, mental or physical disabilities do not
have a place in our community.

Arguing with others members of the community and leadership is to be avoided.

### Community Driven

We value team players not only within our leadership but within our members. If
you notice someone who needs help with anything wow-related, we encourage you to
share your knowledge!

We value sharing your awesome skills and accomplishments! If it's your newest
mount addition, reputation completion, artwork, storytelling, or that awesome
soufflé you just made we want to see the input you have on the world around you!

### Warnings

Violation of our values and terms will be dealt with in private. We typically
work on a “three strikes, you’re out” system, but the severity of which is
dictated at the discretion of the GM, CO-GMS, and officers.

If you see anyone breaking the rules, please feel free to tag a GM, CO-GM, or
officer.
