---
title: Frequently Asked Questions
slug: /
---

### Do you run old content or new content?

We run both and have organized weekly events for old and new content alongside
impromptu get-togethers.

### Do you work on meta achievements?

We have a weekly meta achievement night. It is always scheduled on the calendar
in advance so you can prepare and make sure you are not locked out to the
raids/dungeons we are working on together.

### What's the average player age in your guild?

About 25 - 45 years old. Though we do have players who are both younger and
older than that.

### Do you use discord or another voice program?

Yes. We use Discord heavily. While it is not required it is STRONGLY encouraged.
You don't have to use your mic if it makes you uncomfortable, but being able to
hear instructions during guild events is very helpful!
