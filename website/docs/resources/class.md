---
title: Class Guides and Communites
---

## Death Knight
 * [Blood - Tank (Wowhead article)](https://www.wowhead.com/guide/classes/death-knight/blood/overview-pve-tank)
 * [Frost - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/death-knight/frost/overview-pve-dps)
 * [Unhold - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/death-knight/unholy/overview-pve-dps)
 * [Class Discord](https://discord.gg/acherus)

## Demon Hunter
 * [Havoc - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/demon-hunter/havoc/overview-pve-dps)
 * [Vengence - Tank (Wowhead article)](https://www.wowhead.com/guide/classes/demon-hunter/vengeance/overview-pve-tank)
 * [Class Discord](https://discord.com/invite/zGGkNGC)

## Druid
 * [Balance - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/druid/balance/overview-pve-dps)
 * [Feral - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/druid/feral/overview-pve-dps)
 * [Guardian - Tank (Wowhead article)](https://www.wowhead.com/guide/classes/druid/guardian/overview-pve-tank)
 * [Restoration - Healer (Wowhead article)](https://www.wowhead.com/guide/classes/druid/restoration/overview-pve-healer)
 * [Class Discord](https://discord.com/invite/0dWu0WkuetF87H9H)

## Evoker
 * [Devastation - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/evoker/devastation/overview-pve-dps)
 * [Preservation - Healer (Wowhead article)](https://www.wowhead.com/guide/classes/evoker/preservation/overview-pve-healer)
 * [Class Discord](https://discord.com/invite/JcCFEcmSWD)

## Hunter
 * [Beast Mastery - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/hunter/beast-mastery/overview-pve-dps)
 * [Marksmanship - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/hunter/marksmanship/overview-pve-dps)
 * [Survival - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/hunter/survival/overview-pve-dps)
 * [Class Discord - 1](https://discord.gg/yqer4BX)
 * [Class Discord - 2](https://discord.com/invite/G3tYdTG)

## Mage
 * [Arcane - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/mage/arcane/overview-pve-dps)
 * [Fire - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/mage/fire/overview-pve-dps)
 * [Frost - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/mage/frost/overview-pve-dps)
 * [Class Discord](https://discord.gg/WzYCnbg)

## Monk
 * [Brewmaster - Tank (Wowhead article)](https://www.wowhead.com/guide/classes/monk/brewmaster/overview-pve-tank)
 * [Mistweaver - Healer (Wowhead article)](https://www.wowhead.com/guide/classes/monk/mistweaver/overview-pve-healer)
 * [Windwalker - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/monk/windwalker/overview-pve-dps)
 * [Class Discord](https://discord.gg/0dkfBMAxzTkWj21F)

## Paladin
 * [Holy - Healer (Wowhead article)](https://www.wowhead.com/guide/classes/paladin/holy/overview-pve-healer)
 * [Reribution - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/paladin/retribution/overview-pve-dps)
 * [Protection - Tank (Wowhead article)](https://www.wowhead.com/guide/classes/paladin/protection/overview-pve-tank)
 * [Class Discord](https://discord.gg/hammerofwrath)

## Priest
 * [Discipline - Healer (Wowhead article)](https://www.wowhead.com/guide/classes/priest/discipline/overview-pve-healer)
 * [Holy - Healer (Wowhead article)](https://www.wowhead.com/guide/classes/priest/holy/overview-pve-healer)
 * [Shadow - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/priest/shadow/overview-pve-dps)
 * [Class Discord - 1](https://discord.gg/WarcraftPriests)
 * [Class Discord - 2](https://discord.gg/focusedwill)

## Rogue
 * [Assassionation - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/rogue/assassination/overview-pve-dps)
 * [Outlaw - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/rogue/outlaw/overview-pve-dps)
 * [Subtlety - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/rogue/subtlety/overview-pve-dps)
 * [Class Discord](https://discord.gg/0h08tydxoNhfDVZf)

## Shaman
 * [Elemental - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/shaman/elemental/overview-pve-dps)
 * [Enhancement - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/shaman/enhancement/overview-pve-dps)
 * [Restoration - Healer (Wowhead article)](https://www.wowhead.com/guide/classes/shaman/restoration/overview-pve-healer)
 * [Class Discord - 1](https://discord.com/invite/0VcupJEQX0HuE5HH)
 * [Class Discord - 2](https://discord.com/invite/AcTek6e)


## Warlock
 * [Affliection - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/warlock/affliction/overview-pve-dps)
 * [Demonology - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/warlock/demonology/overview-pve-dps)
 * [Desctuction - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/warlock/destruction/overview-pve-dps)
 * [Class Discord](https://discord.gg/0onXDymd9Wpc2CEu)

## Warrior
 * [Arms - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/warrior/arms/overview-pve-dps)
 * [Fury - DPS (Wowhead article)](https://www.wowhead.com/guide/classes/warrior/fury/overview-pve-dps)
 * [Protection - Tank (Wowhead article)](https://www.wowhead.com/guide/classes/warrior/protection/overview-pve-tank)
 * [Class Discord](https://discord.gg/Skyhold)