---
title: Addons
---


 * [Curseforge App](https://download.curseforge.com/) - Keep all your addons up to date with one easy desktop app.
## User Interface
 * [ElvUI](https://www.tukui.org/download.php?ui=elvui) - Make your own or import a UI to suite your needs
 * [Bartender4](https://www.curseforge.com/wow/addons/bartender4) - Move your action bars where you want.
 * [WeakAuras2](https://www.curseforge.com/wow/addons/weakauras-2) - Customizable ability trackers for each class. Create you own or Import premade ones.
 * [Details](https://www.curseforge.com/wow/addons/details) - Damage and Healing meters
 * [Tomtom](https://www.curseforge.com/wow/addons/tomtom) - In game GPS
 * [Clique](https://www.curseforge.com/wow/addons/clique) - Easy way to make mouseover macros for all class. Especially good for healers
 * [Decursive]( https://www.curseforge.com/wow/addons/decursive) - Provides a UI to “cleanse” any ailment from party/raid members without having to select them. Works for any class with some sort of “cleanse” ability.
 * [KUI nameplates](https://www.curseforge.com/wow/addons/kuinameplates) - Customizable Friend/Enemy Nameplates
 * [Plater nameplates](https://www.curseforge.com/wow/addons/plater-nameplates) - Customizable Nameplates
## Dungeon/Raiding
 * [Deadly Boss Mods](https://www.curseforge.com/wow/addons/deadly-boss-mods) - Raid and Dungeons alerts
 * [GTFO](https://www.curseforge.com/wow/addons/gtfo) - Big alerts for when you’re standing in bad stuff.
 * [Astral Keys](https://www.curseforge.com/wow/addons/astral-keys) - Tracks and transmits Mythic+ keys for guild members and characters to see.
 * [Raider.IO](https://www.curseforge.com/wow/addons/raiderio) - The addon to go along with the Raid and Mythic+ Rankings site, https://raider.io
 * [Simulationcraft](https://www.curseforge.com/wow/addons/simulationcraft) - Works in conjunction with Raidbots to simulate your toon’s potential.
 * [MythicPlusTimer](https://www.curseforge.com/wow/addons/mythicplustimer) - Gives Mythic+ information such as: Keystone, Affixes, Time, Bosses, and Percent.
## Achievements/Tracking
 * [Silverdragon](https://www.curseforge.com/wow/addons/silver-dragon) - Tracks and displays near by rare mobs, rare events, and chests.
 * [Handynotes & Handynotes - Shadowlands](https://www.curseforge.com/wow/addons/handynotes) - Helps track rare mobs, rare events, and chests. Also gives information on how to summon, activate, or open previously said items.
 * [Rarity](https://www.curseforge.com/wow/addons/rarity) - Tracks and reports how many times you have attempted to get certain rare in game items whether it be mounts, pets, and toys.
 * [Simply Armory](https://www.curseforge.com/wow/addons/simple-armory) - An addon that allows you to export collections unavailable through the Blizzard API. Their website, http://simplearmory.com, is where you can use to see all your achievements, collections (mounts, pets, toys), reputations, etc
 * [Instance Achievement Tracker](https://www.curseforge.com/wow/addons/instance-achievement-tracker) - Helps track criteria of achievements is being met or failed. Also gives strategies on each achievement available in a dungeon
 * [Overachiever](https://www.curseforge.com/wow/addons/overachiever) - Tools and tweaks to make the lives of players seeking achievements a little bit easier
 * [Azeroth Auto Pilot](https://www.curseforge.com/wow/addons/azeroth-auto-pilot) - Leveling addon that tells you where to go, who to talk to, and  almost everything except push WASD
## Chat & Communication
 * [Emote Splitter](https://www.curseforge.com/wow/addons/emote-splitter) - Removes the character limit from your chatbox and lets you paste or type as much text as you want.
 * [Badboy](https://www.curseforge.com/wow/addons/bad-boy) - Blocks chat spam from gold, hack, phishing, account trading, etc.
## Quality of Life
 * [Adibags/Adibags](https://www.curseforge.com/wow/addons/adibags) - Bad addon to simplify your bag with customizable categories
 * [Can I Mog It?](https://www.curseforge.com/wow/addons/can-i-mog-it) - Tells you whether or not you have an item in your appearances.
 * [GatherMate2](https://www.curseforge.com/wow/addons/gathermate2) / [GatherMate2_Data](https://www.curseforge.com/wow/addons/gathermate2_data) - Displays on your minimap previously gathered nodes. Tracks herbs, ores, and fish.
 * [Identity-2](https://www.curseforge.com/wow/addons/identity-2) - Allows one to display who they are even on an alt through text channels. Usually used by GMs and Officers to denote who they are to newer members.
 * [Narcissus](https://www.curseforge.com/wow/addons/narcissus) - Screenshot addon to make and take dynamic screenshots.
 * [Total RP 3](https://www.curseforge.com/wow/addons/total-rp-3) - Roleplay addon to completely customize your character’s backstory, features, and help immerse yourself into your toon.
 * [War Plan](https://www.curseforge.com/wow/addons/war-plan) - Helps with follow table. Gives best guess on success or failure.
 * [World Quests List](https://www.curseforge.com/wow/addons/world-quests-list) - Tracks and displays all current available World Quests with info such as: Location, Reward, Faction Rep, and Time available to complete.
 * [ExtraQuestButton](https://www.curseforge.com/wow/addons/extraquestbutton) - ExtraQuestButton adds a button that will show the quest item for the closest quest with an item, it is basically the ExtraActionButton of quests
 * [OmniCC](https://www.curseforge.com/wow/addons/omni-cc) - OmniCC is an addon that adds text to items, spell and abilities that are on cooldown
## PVP
 * [BattleGroundEnemies](https://www.curseforge.com/wow/addons/battlegroundenemies) - A lightweight Addon to show you all enemies, allies and important player-specific information in a battleground
 * [OmniBar](https://www.curseforge.com/wow/addons/omnibar) - OmniBar is an extremely lightweight addon that tracks enemy cooldowns
## Battle Pets
 * [Rematch](https://www.curseforge.com/wow/addons/rematch) - Rematch is a pet journal alternative for managing pets and teams
