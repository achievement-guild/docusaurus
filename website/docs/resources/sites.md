---
title: Websites
---

## WoW General
 * [Wowhead](https://www.wowhead.com/)              - Great WoW resources to help with almost anything WoW related
 * [Icyveins](https://www.icy-veins.com/wow/)       - WoW resources

## WoW Addons
 * [CurseForge](https://www.curseforge.com/)        - Most common place to get your WoW addons.
 * [Wago.io](https://wago.io/)                      - Import/Export strings for Weakauras, Elvui, and any other addons that can receive custom strings.

## Battle Pets
 * [Xu-fu](https://www.wow-petguide.com/)           - Online guides for battle pets
 * [Warcraft Pets](https://www.warcraftpets.com/)   - Tracks and guides for collecting pets

## WoW PVE
 * [Raidbots](https://www.raidbots.com/simbot)      - Works with Simulationcraft to compare new gear, check dps, and see what your toon can do.
 * [Raider.io](https://raider.io/)                  - Raid and Mythic+ rankings.
 * [Warcraft Logs](https://www.warcraftlogs.com/)   - Logs for your raid parsing.
 * [WowAnalyzer](https://www.wowanalyzer.com/)      - Analyze your logs for advice on what could be done better.
 * [Subcreation](https://subcreation.net/)          - Mythic+ and Raid class tiering and builds based on leaderboards.
 * [Mythic Trap](https://www.mythictrap.com/en)     - Raid guide will gif visuals to help reference without needed a full video.
 * [Tank Notes](https://tanknotes.com/)             - Raid Tanking guide will gif visuals to help reference without needed a full video.


## WoW PVP
 * [Murlok.io](https://murlok.io/)                  - Provides 2v2, 3v3, RBG, and Mythic+ class guides based on player data.
 * [Check PVP](https://check-pvp.fr/)               - PVP player rating for current and prior seasons.