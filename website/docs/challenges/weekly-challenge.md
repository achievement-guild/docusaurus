---
title: Weekly Challenge
slug: /chalenges/
---

## August 2nd - 8th

#### Objective:

Fill 4 slots in your Great Vault

#### Requirements:

With the start of Season 4 we thought it would be an easy accomplishment. Get a
minimum of 4 slots filled before the end of the week. Doing this for the weekly
will not only get you points, but also make sure you submit it for great vault
points as well. It's like easy double points. Once you have your 4 slots filled
simply take a screenshot like normal, with your toon and it's vault in the
image, and upload it to the `#submission-entry`.

#### Rewards:

Flat 10 points​
