---
title: Monthly Challenge
---

## December

Spread some Winter Veil cheer and sign up for Secret Santa!

#### Objective:

There is so much to do right now. Getting to 70, getting all the dragon riding glyphs, prepping for professions, mythic+, and raid. We decided to take a little pressure off those activities. We will be doing a Secret Santa.

#### Requirements:

The [Secret Santa form](https://forms.gle/hACNRBPdh84hX6tp7) is here. You will need to log in with a google email address, the email can be a temporary it is there so each person can respond only once, then fill it out by **December 11th**. On **December 12th** everyone that put in a response will receive the response of the person they are to get a gift for. Make sure to read over their response carefully and try to adhere to their answers as closely as possible. You can also send as many or just one as long as the gift(s) meet the requirements below.

**Gifts must have have at the LEAST a total value of 10k gold**
**You can send multiple items, but the total cost should be a minimum of 10k gold**

Your gift(s) should be sent between **December 20th - 25th. NO EARLIER OR LATER!**

#### Reward:

It the season of giving not receiving! As such no points will be awarded for this Monthly.
