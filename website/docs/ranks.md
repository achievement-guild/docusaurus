---
title: Guild Ranks
---

## Guild Ranks

### RANK: LEGENDARY
#### POINTS REQUIRED: 1000
-   2000 gold daily repair 
-   Schedule Discord and Guild Calendar events 
-   Invite privleges
-   Can give 1 rank point to anyone once a week via the `#refer` discord channel
-   1 guild bank withdraw per day
-   Enter into a monthly raffle for 50k gold.

### RANK: EPIC
#### POINTS REQUIRED: 500
-   2000 gold daily repair
-   Invite privleges
-   Can give 1 rank point to anyone once a week via the `#refer` discord channel
-   1 guild bank withdraw per day

### RANK: RARE
#### POINTS REQUIRED: 250
-   1000 gold daily repair ​
-   1 guild bank withdraw per day

### RANK: UNCOMMON
#### POINTS REQUIRED: 75
-   500 gold daily repair ​

### ​​​RANK: COMMON
#### RANK ASSIGNED WHEN JOINING
-   250 gold daily repair 

### RANK: ALT
#### ANY CHARACTER THAT ISN'T A PRIMARY CHARACTER
-   200 gold daily repair ​
