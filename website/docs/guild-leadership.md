---
title: Guild Leadership
---

## Guild Master
 * [Galafen](https://worldofwarcraft.com/en-us/character/us/wyrmrest-accord/Galafen)

## Co - Guild Master
 * [Ruckuspaw](https://worldofwarcraft.com/en-us/character/us/wyrmrest-accord/ruckuspaw)

## Officers
 * [Blazedale](https://worldofwarcraft.com/en-us/character/us/wyrmrest-accord/Blazeofglory)
 * [Diamandala](https://worldofwarcraft.com/en-us/character/us/wyrmrest-accord/diamandala)
 * [Elzandra](https://worldofwarcraft.com/en-us/character/us/wyrmrest-accord/Elzandra)
