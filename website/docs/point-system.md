---
title: Point System
---

![wow-book](/img/wowbook.png)
Achievement members can earn points on their main character to rank up. Points can be earned in many different ways, but all of them are intended to be through means that really help to strengthen our community at its core.

 > Please note, special circumstances may lower or increase the point value at
the GM, CO-GM, or Officer’s discretion, but this is a baseline for our point
system.

Keep in mind that the GMs and officers of Achievement cannot always be online,
and therefore won’t always be aware of completed tasks. Because of this, if you
see someone doing something nice for someone else, be sure to post about it in
the #refer channel in discord for evaluation!

In the end, points are awarded at the discretion of the GMs and Officers, and
might not be rewarded for each and every single task (especially if it is a task
that has been repeated often in a short time frame by the same person). However,
we encourage you to keep being friendly and doing kind things for others, even
if it doesn’t earn you points. After all, our goal is to make Achievement a
welcoming place to be!

 > More ways to earn points may be added in the future.


### 3 Points 
 * Attending a Guild Event 
 * Participating in Mythic Monday - At least 2 Guild Members need to run together, refer your guildmate as you would a normal referral once the dungeon is complete, this will cap your dungeon referral points for the day

### 1 Point 
 * Referrals - You can Earn up to 3 Referral Points a day, which must be assigned by a fellow guild member. Examples of acceptable referral reasons include : 
   * Join a Guild Member in a current dungeon
   * Assist a Guild Member in killing a rare 
   * Assist a Guild Member with a quest they cannot do on their own
   * Working together with guild members on a common goal (i.e. obtaining a rare pet, drop or mount) 
 * Legendary/Epic Point -  Once a week, Legendary and Epic members can assign a point to another member for any reason. 


### How Referrals Work: 

We have a `#Refer` channel in Discord. This is where you refer your fellow guild mates for helping or joining you. Type @*(their Discord name)* and then what the referral is for - (see the points section for examples)  i.e. 

```Diamandala:  @Blox @Ruckuspaw @Refill @Galafen for the M +12```

Then someone in the group will need to refer you for the whole group to earn a point. 

```Blox: @Diamandala for healing the +12```

Now your group has all been referred and points will be added to your profile!  Please DO NOT refer your group if it has already been referred by someone else. Duplicate points will not be added and it clutters up the process. 

It is also important that your Discord name matches the name of your main character so that points can be referred to the correct account. Please make sure you do this when you’ve added yourself to the bot system. 
