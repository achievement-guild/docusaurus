module.exports = {
    docs: [
        {
            type: 'category',
            label: 'Information',
            items: [
                'faq',
                'rules',
                'ranks',
                'point-system',
                'guild-leadership',
            ],
        },
        // {
        //     type: 'category',
        //     label: 'Challenges',
        //     items: [
        //         // 'challenges/weekly-challenge',
        //         'challenges/monthly-challenge',
        //     ],
        // },
        // 'motm/member-of-the-month',
        {
            type: 'category',
            label: 'Resources',
            items: [
                'resources/sites',
                'resources/class',
                'resources/addons',
                'resources/social',
            ],
        },
    ],
};
